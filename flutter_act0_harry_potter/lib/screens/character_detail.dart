import 'package:flutter/material.dart';

import '../models/character.dart';

class CharacterDetail extends StatelessWidget {
  const CharacterDetail({super.key, required this.character});

  final Character character;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text(character.name),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Hero(
              tag: character.name,
              child: Image.network(character.url),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    for (int i = 1; i <= 5; i++)
                      (character.stars >= i)
                          ? const Icon(Icons.star)
                          : const Icon(Icons.star_border_sharp),
                  ],
                ),
                Text("${character.reviews} reviews"),
              ],
            ),
            Text(character.name,
                style: const TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    const Icon(Icons.fitness_center),
                    const Text("Força"),
                    Text("${character.strength}"),
                  ],
                ),
                Column(
                  children: [
                    const Icon(Icons.auto_fix_normal),
                    const Text("Màgia"),
                    Text(character.magic.toString()),
                  ],
                ),
                Column(
                  children: [
                    const Icon(Icons.speed),
                    const Text("Velocitat"),
                    Text(character.speed.toString()),
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
