import 'package:flutter/cupertino.dart';

import '../models/actividad.dart';

class ActividadProvider extends ChangeNotifier {
  List<Actividad> _actividades = [
    Actividad(
      tipo: "Running",
      fecha: "Ayer, 18:20",
      longitud: 7.3,
    ),
    Actividad(
      tipo: "Running",
      fecha: "15 Oct 2022, 13:45",
      longitud: 6.55,
    ),
    Actividad(
      tipo: "Running",
      fecha: "10 Oct 2022, 19:02",
      longitud: 7.3,
    ),
  ];

  List<Actividad> get actividades => _actividades;

  void agregarActividad(Actividad nuevaActividad) {
    _actividades.add(nuevaActividad);
    notifyListeners(); // Notificar a los oyentes que la lista de actividades ha cambiado
  }
}