import 'package:flutter/material.dart';
import 'package:flutter_act1_crear_pantalla_disseny/provider/actividad_provider.dart';
import 'package:provider/provider.dart';

class Detalles extends StatefulWidget {
  const Detalles({Key? key, required this.nombre}) : super(key: key);

  final String nombre;

  @override
  _DetallesState createState() => _DetallesState();
}

class _DetallesState extends State<Detalles> {
  double height = 150;
  double weight = 50;

  @override
  Widget build(BuildContext context) {
    final actividadProvider = Provider.of<ActividadProvider>(context);

    double sumKilometers = 0;
    for (var actividad in actividadProvider.actividades) {
      sumKilometers += actividad.longitud;
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          'My Profile',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Center(
            child: Column(
              children: [
                SizedBox(height: 15),
                CircleAvatar(
                  radius: 120,
                  backgroundImage: NetworkImage(
                      'https://randomuser.me/api/portraits/women/44.jpg'),
                ),
                Text(
                  widget.nombre,
                  style: TextStyle(fontSize: 45, color: Colors.grey),
                ),
                Text(
                  "since 20 April 2020",
                  style: TextStyle(fontSize: 15, color: Colors.grey),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _buildSquare(
                        context, Icons.access_time, 'Time', '2h 4\''),
                    _buildSquare(
                        context, Icons.location_on, 'km', '$sumKilometers'),
                    _buildSquare(context, Icons.directions_run, 'Activities',
                        '${actividadProvider.actividades.length}'),
                  ],
                ),
                SizedBox(height: 20),
                Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Height',
                          style: TextStyle(fontSize: 16),
                        ),
                        Expanded(
                            child: Slider(
                              value: height,
                              onChanged: (double value) {
                                setState(() {
                                  height = value;
                                });
                              },
                              min: 0,
                              max: 225,
                              divisions: 225,
                              label: 'Height',
                            )),
                        Text(
                          '${height.toStringAsFixed(0)} cm',
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Column(
                  children: [
                    Row(
                      children: [
                        Text(
                          'Weight',
                          style: TextStyle(fontSize: 16),
                        ),
                        Expanded(
                          child: Slider(
                            value: weight,
                            onChanged: (double value) {
                              setState(() {
                                weight = value;
                              });
                            },
                            min: 0,
                            max: 150,
                            divisions: 150,
                            label: 'Weight',
                          ),
                        ),
                        Text(
                          '${weight.toStringAsFixed(0)} kg',
                          style: TextStyle(fontSize: 16),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildSquare(BuildContext context, IconData icon, String title,
      String subtitle) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        color: Theme.of(context).focusColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, size: 40),
          SizedBox(height: 5),
          Text(
            title,
            style: TextStyle(fontSize: 16),
          ),
          Text(
            subtitle,
            style: TextStyle(fontSize: 16),
          ),
        ],
      ),
    );
  }
}