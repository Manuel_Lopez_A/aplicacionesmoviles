import 'package:flutter/material.dart';

class Actividad extends StatelessWidget {
  final String tipo;
  final String fecha;
  final double longitud; // Cambiado a double

  const Actividad({
    Key? key, // Añadido el parámetro key
    required this.tipo,
    required this.fecha,
    required this.longitud,
  }) : super(key: key); // Inicialización de la superclase con la clave proporcionada

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(9.0),
        child: Row(
          children: [
            Container(
              width: 30, // Ancho del contenedor
              height: 30, // Alto del contenedor
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.black, width: 2.0),
              ),
              child: const Center(
                child: Icon(
                  Icons.directions_run,
                  size: 20,
                ),
              ),
            ),
            const SizedBox(width: 10), // Espacio entre el icono y el texto
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tipo, // Título de la actividad
                  style: const TextStyle(
                    fontSize: 24, // Tamaño de fuente del texto del Card
                  ),
                ),
                const SizedBox(height: 2), // Espaciado entre los textos
                Text(
                  fecha, // Subtítulo de la actividad
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.black45, // Tamaño de fuente del segundo texto del Card
                  ),
                ),
              ],
            ),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  '${longitud.toString()} km', // Distancia de la actividad
                  style: const TextStyle(
                    fontSize: 24, // Tamaño de fuente del tercer texto del Card
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}