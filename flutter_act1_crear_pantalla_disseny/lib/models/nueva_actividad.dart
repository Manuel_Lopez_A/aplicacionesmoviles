import 'package:flutter/material.dart';
import 'package:flutter_act1_crear_pantalla_disseny/models/actividad.dart';
import 'package:provider/provider.dart';

import '../provider/actividad_provider.dart';

class NuevaActividad extends StatefulWidget {
  @override
  _NuevaActividadState createState() => _NuevaActividadState();
}

class _NuevaActividadState extends State<NuevaActividad> {
  final TextEditingController _nombreController = TextEditingController();
  final TextEditingController _tiempoController = TextEditingController();
  final TextEditingController _distanciaController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nueva Actividad'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: _nombreController,
              decoration: InputDecoration(labelText: 'Nombre de la actividad'),
            ),
            SizedBox(height: 20),
            TextField(
              controller: _tiempoController,
              decoration: InputDecoration(labelText: 'Tiempo (hh:mm:ss)'),
            ),
            SizedBox(height: 20),
            TextField(
              controller: _distanciaController,
              decoration: InputDecoration(labelText: 'Distancia (km)'),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                final actividad = Actividad(
                  tipo: _nombreController.text,
                  fecha: _tiempoController.text, // Asignación del campo tiempo como fecha
                  longitud: double.parse(_distanciaController.text),
                );
                Provider.of<ActividadProvider>(context, listen: false).agregarActividad(actividad);
                Navigator.pop(context);
              },
              child: Text('Insertar nueva actividad'),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _nombreController.dispose();
    _tiempoController.dispose();
    _distanciaController.dispose();
    super.dispose();
  }
}