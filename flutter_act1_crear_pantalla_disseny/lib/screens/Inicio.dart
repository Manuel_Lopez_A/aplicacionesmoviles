import 'package:flutter/material.dart';
import 'package:flutter_act1_crear_pantalla_disseny/models/actividad.dart';
import 'package:flutter_act1_crear_pantalla_disseny/models/detalles.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../models/nueva_actividad.dart';
import '../provider/actividad_provider.dart';

class Inicio extends StatelessWidget {
  const Inicio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              color: Colors.white,
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => const AlertDialog(
                    content: Text("HOLA"), // Envuelve el texto en un widget Text,
                  ),
                );
              },
            );
          },
        ),
        title: const Center(
          child: Text(
            'Fitness Time',
            style: TextStyle(color: Colors.white),
          ),
        ),
        actions: const [
          CircleAvatar(
            backgroundImage: NetworkImage('https://randomuser.me/api/portraits/women/44.jpg'),
          ),
          SizedBox(width: 16), // Espacio entre el avatar y los otros elementos
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Hola Diana,",
              style: TextStyle(
                color: Colors.grey, // Cambia el color del texto a negro
                fontSize: 40, // Cambia el tamaño de la fuente
              ),
            ),
            const Text("Come 5 veces al día y permanece hidratada durante el día"),
            Padding(
              padding: const EdgeInsetsDirectional.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const Detalles(nombre: "Antònia Font")), // Navega a la pantalla de detalles
                      );
                    },
                    child: const Text(
                      "Más detalles:",
                      style: TextStyle(
                        fontSize: 14, // Cambia el tamaño de la fuente
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NuevaActividad()), // Navega a la pantalla de nueva actividad
                      );
                    },
                    child: const Text(
                      "Nueva Actividad", // Nuevo texto para añadir nueva actividad
                      style: TextStyle(
                        fontSize: 14, // Tamaño de la fuente
                        color: Colors.lightBlueAccent,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            const Text(
              "Últimas actividades:",
            ),
            Flexible(
              child: Consumer<ActividadProvider>(
                builder: (context, actividadProvider, _) {
                  final actividades = actividadProvider.actividades;
                  return ListView.builder(
                    shrinkWrap: true,
                    itemCount: actividades.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Actividad(
                        tipo: actividades[index].tipo,
                        fecha: actividades[index].fecha,
                        longitud: actividades[index].longitud,
                      );
                    },
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Center(
                child: Column(
                  children: [
                    CircularPercentIndicator(
                      backgroundWidth: 15.0,
                      lineWidth: 15.0,
                      radius: 65,
                      percent: 0.7,
                      progressColor: Colors.blue, // Color azul
                      circularStrokeCap: CircularStrokeCap.round, // Bordes redondeados
                      center: const Text(
                        "70.0%",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    const Text(
                      "Objetivo mensual",
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Buscar',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Perfil',
          ),
        ],
      ),
    );
  }
}