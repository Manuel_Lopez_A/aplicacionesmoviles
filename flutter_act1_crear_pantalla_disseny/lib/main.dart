import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_act1_crear_pantalla_disseny/provider/actividad_provider.dart';
import 'package:flutter_act1_crear_pantalla_disseny/screens/Inicio.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ActividadProvider(), // Proporciona una instancia de ActividadProvider
      child: MaterialApp(
        title: 'Act 1 - Manuel López',
        home: MyHomePage(title: 'Fitness Time'),
        theme: ThemeData(
          primaryColor: const Color(0xFFF15E9E),
          appBarTheme: const AppBarTheme(
            color: Color(0xFFD3008C), // Color de la barra de la aplicación
            iconTheme: IconThemeData(color: Colors.white), // Color del icono del menú
          ),
          fontFamily: GoogleFonts.montserrat().fontFamily,
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Inicio();
  }
}