package com.example.listadelacompra;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class __BuyList_Activity__Sqlite extends AppCompatActivity {
    private static ArrayList<Product> almacen = new ArrayList<>();
//    ProductosBBDD stockProductos = new ProductosBBDD(this, "stockProductos", null, 1);
    private static final String KEY_PRODUCT_LIST = "buyList";
    private static AA_BuyList adaptador;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3_buylist);

        /** -------------------ADPTADOR---------------------**/
        adaptador = new AA_BuyList(this, almacen);
        ListView listaCompra = findViewById(R.id.listShopping);
        listaCompra.setAdapter(adaptador);

        // Verificar si hay un Bundle
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            Log.e("array", "recogiendolo desde buyList");
            // Recuperar la lista generada del Bundle
            ArrayList<Product> listaGenerada = bundle.getParcelableArrayList(KEY_PRODUCT_LIST); // aquí es donde me peta en esta linea
            // Añadir los productos de la lista generada a la lista 'almacen'
            almacen.clear();
            almacen.addAll(listaGenerada);
        }

        configurarBotones();
    }

    private void configurarBotones() {
        //BOTÓN BORRAR TODO
        Button returnMain = findViewById(R.id.btnVaciar);
        returnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                estasSeguro();
            }
        });
        //BOTÓN GUARDAR Y SALIR
        Button saveButton = findViewById(R.id.btnSaveList);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void estasSeguro() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.advertenciaBorrado);
        builder.setMessage(R.string.mensajeBorrado);
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Si el usuario hace clic en "Sí", borra la lista y finaliza la actividad
                almacen.clear();
                adaptador.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Si el usuario hace clic en "No", cierra el diálogo
                dialogInterface.dismiss();
               }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        actualizarAdapter();
    }
//
//    private void actualizarAdapter() {
//        SQLiteDatabase db = stockProductos.getWritableDatabase();
//        almacen.clear();
//        almacen.addAll(stockProductos.llenarStockProductos(db));
//        db.close();
//        // Notificar al adaptador que los datos han cambiado
//        // ((AA_BuyList) ((ListView) findViewById(R.id.listShopping)).getAdapter()).notifyDataSetChanged();
//        ListView listProducts = findViewById(R.id.listShopping);
//        AA_BuyList productListAdapter = (AA_BuyList) listProducts.getAdapter();
//        productListAdapter.notifyDataSetChanged();
//    }
}