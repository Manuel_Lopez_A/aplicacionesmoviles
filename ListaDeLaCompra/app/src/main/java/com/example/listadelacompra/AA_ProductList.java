package com.example.listadelacompra;

import android.app.Activity;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class AA_ProductList extends ArrayAdapter<Product> {

    MediaPlayer mp = MediaPlayer.create(this.getContext(), R.raw.latex_gloves);
    private final Activity activity;
    private final ArrayList<Product> almacen;
    private final Productos_BBDD dbHelper;

    public AA_ProductList(Activity activity, ArrayList<Product> stock, Productos_BBDD dbHelper) {
        super(activity, R.layout.product_1_list_view_item, stock);
        this.activity = activity;
        this.almacen = stock;
        this.dbHelper = dbHelper;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View item = inflater.inflate(R.layout.product_1_list_view_item, null);

        TextView productoNombre = item.findViewById(R.id.lblNombre);
        TextView productoDesc = item.findViewById(R.id.lblDescripcion);

        ImageView buttonDelete = item.findViewById(R.id.btn_eliminar);
        ImageView miniatura = item.findViewById((R.id.mini_photoProduct));

        productoNombre.setText(almacen.get(position).getNombre());
        productoDesc.setText(String.valueOf(almacen.get(position).getDescripcion()));
        cargarImagenDesdeRuta(almacen.get(position).getRutaImagen(), miniatura);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarProducto(position);
            }
        });

        return item;
    }

    private void eliminarProducto(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage(R.string.productoEliminar)
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mp.start();
                        eliminarProductoDeBBDD(almacen.get(position).getIdSql());
                        almacen.remove(position);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton(R.string.deny, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        builder.create().show();
    }

    private void eliminarProductoDeBBDD(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        dbHelper.deleteProducto(db, id);
        db.close();
    }
    private void cargarImagenDesdeRuta(String rutaImagen, ImageView imagen) {
        // Verificar si hay una ruta de imagen válida
        if (rutaImagen != null && !rutaImagen.isEmpty()) {
            Uri uri = Uri.parse(rutaImagen);
            Log.e("FOTO", uri.toString());
            Glide.with(activity)
                    .load(uri)
                    .fitCenter()  // Ajusta la escala de la imagen
                    .into(imagen);
        }
    }
}