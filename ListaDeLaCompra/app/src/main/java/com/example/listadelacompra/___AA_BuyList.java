package com.example.listadelacompra;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class ___AA_BuyList extends ArrayAdapter {

    Activity activity;
    ArrayList<Product> almacen = new ArrayList<>();


    public ___AA_BuyList(Activity activity, ArrayList<Product> stock) {
        /** ---ACTIVITY-----------LAYOUT--------------ARRAY--**/
        super(activity, R.layout.___item_view_products_buy_list, stock);
        this.activity = activity;
        this.almacen = stock;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Obtiene el servicio de inflado de diseños desde la actividad
        LayoutInflater inflater = activity.getLayoutInflater();
        // Infla el diseño personalizado para cada elemento de la lista
        View item = inflater.inflate(R.layout.product_3_buylist_view_item, null);

        // Obtiene las referencias a los TextViews dentro del diseño inflado
        TextView productoNombre = item.findViewById(R.id.lblNombre);
        TextView productoDesc = item.findViewById(R.id.lblDescripcion);

        // Establece el texto de los TextViews con los datos del producto en la posición actual
        productoNombre.setText(almacen.get(position).getNombre().toString());
        productoDesc.setText(String.valueOf(almacen.get(position).getDescripcion()));


        /** ----------------------DELETE----------------------------**/
        Button buttonDelete = item.findViewById(R.id.buttonDelete);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                almacen.remove(position);
                notifyDataSetChanged(); // notifica que se han cambiado datos
            }
        });
        /** ----------------------DELETE----------------------------**/


        //return super.getView(position, convertView, parent);
        return item;
    }

}
