package com.example.listadelacompra;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;

import java.util.Locale;

public class Preferences extends PreferenceActivity {
//	String valor;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferencias);

		// Escucha el cambio de preferencias para aplicar el cambio de idioma inmediatamente
		findPreference("languagePref").setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				// Aplica el cambio de idioma
				cambiaIdioma(newValue.toString());

				// Devuelve true para indicar que se ha gestionado el cambio correctamente
				return true;
			}
		});
	}

	@Override
	protected void onRestart() { // mejor onRestart que onSTop
		// TODO Auto-generated method stub
		super.onRestart();

		/** Per guardar dades a preferencies **/
//		SharedPreferences prefs = getActivity().getPreferences(Context.MODE_PRIVATE);

	}

	private Activity getActivity() {
		return this;
	}

	private void cambiaIdioma(String language) {
		Locale locale;
		switch (language) {
			case "en":
				locale = new Locale("en");
				break;
			case "es":
				locale = new Locale("es");
				break;
			case "ca":
				locale = new Locale("ca");
				break;
			default:
				locale = Locale.getDefault();
				break;
		}
		Locale.setDefault(locale);
		Configuration configuration = new Configuration();
		configuration.locale = locale;
		getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
		Log.e("idioma cambiado:", language);

		// Vuelve a crear la actividad para aplicar los cambios
		recreate();
	}
}
