package com.example.listadelacompra;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Product_Activity extends AppCompatActivity {
    int idProducto = -1;
    Productos_BBDD stockProductos = new Productos_BBDD(this, "stockProductos", null, 1);
    Button buttonRemoveImage, buttonUploadImage, buttonReturn, buttonUpdate;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private String DIRECTORIO = "root";
    private Uri imageUri;
    ProgressDialog progressDialog;
//    private static final int COD_SEL_IMAGE = 300;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__updateproduct);

        Bundle bundle = this.getIntent().getExtras();
        progressDialog = new ProgressDialog(this);
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();

        TextView titulo = findViewById(R.id.tituloProducto);
        EditText nombre = findViewById(R.id.datosNombre);
        EditText descripcion = findViewById(R.id.datosDescripcion);
        EditText stockMin = findViewById(R.id.datosStockMin);
        EditText stockAct = findViewById(R.id.datosStockActual);
        ImageView imagen = findViewById(R.id.product_photo);

        titulo.setText(bundle.getString("nombre"));
        nombre.setText(bundle.getString("nombre"));
        descripcion.setText(bundle.getString("descripcion"));
        stockMin.setText(String.valueOf(bundle.getDouble("stockMinimo")));
        stockAct.setText(String.valueOf(bundle.getDouble("stockActual")));
        cargarImagenDesdeRuta(bundle.getString("rutaImagen"), imagen);

        idProducto = bundle.getInt("idSql");

        // BOTONES --->
        buttonReturn = findViewById(R.id.buttonVolver);
        buttonUpdate = findViewById(R.id.buttonInsertar);
        buttonUploadImage = findViewById((R.id.btn_photo));
        buttonRemoveImage = findViewById(R.id.btn_remove_photo);


        // FUNCIONES BOTONES --->
        buttonUploadImage.setOnClickListener(v -> seleccionarImagen());
        buttonRemoveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // eliminar foto de la base de datos...
                Toast.makeText(Product_Activity.this, "Foto eliminada", Toast.LENGTH_SHORT).show();
            }
        });
        buttonReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent resultadoIntent = new Intent();
                setResult(RESULT_CANCELED, resultadoIntent);
                finish();
            }
        });
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues nuevosValores = new ContentValues();

                nuevosValores.put("nombre", nombre.getText().toString());
                nuevosValores.put("descripcion", descripcion.getText().toString());
                nuevosValores.put("stockMin", (stockMin.getText().toString()));
                nuevosValores.put("stockActual",(stockAct.getText().toString()));

                SQLiteDatabase db = stockProductos.getWritableDatabase();
                stockProductos.updateProducto(db, nuevosValores, idProducto);
                db.close();
                Toast.makeText(getApplicationContext(), "Producto actualizado con éxito", Toast.LENGTH_SHORT).show();

                Intent resultadoIntent = new Intent();
                setResult(RESULT_OK, resultadoIntent);
                finish();
            }
        });
    }
    private void seleccionarImagen() {
        // Crear un intent para seleccionar una imagen desde la galería
        Intent pickIntent = new Intent(Intent.ACTION_PICK);
        pickIntent.setType("image/*");

        // Crear un intent para tomar una foto
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Título que se mostrará en el selector de acciones
        String pickTitle = "Seleccionar o tomar una foto";

        // Crear un intent chooser que muestra las opciones de selección de galería o cámara
        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);

        // Agregar la opción de tomar foto al intent chooser
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePhotoIntent});

        // Iniciar la actividad para seleccionar o tomar una foto
        startActivityForResult(chooserIntent, 1);
    }

    private void cargarImagenDesdeRuta(String rutaImagen, ImageView imagen) {
        // Verificar si hay una ruta de imagen válida
        if (rutaImagen != null && !rutaImagen.isEmpty()) {
            Uri uri = Uri.parse(rutaImagen);
            Log.e("FOTO", uri.toString());
            Glide.with(this)
                    .load(uri)
                    .fitCenter()  // Ajusta la escala de la imagen
                    .into(imagen);
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1 && data != null && data.getData() != null) {
                imageUri = data.getData();
                subirImagen();
            } else if (requestCode == 1 && data.getExtras() != null) {
                Bundle extras = data.getExtras();
                // La imagen capturada está en el objeto extras
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                // Guardar la imagen en local
                saveImageLocally(imageBitmap);
                // Ahora puedes trabajar con la imagenBitmap
                // Por ejemplo, mostrarla en un ImageView
                ImageView imageView = findViewById(R.id.product_photo);
                imageView.setImageBitmap(imageBitmap);

                // O subirla a Cloud Storage
                subirImagenCapturada(imageBitmap);
            }
        }
    }
    private void saveImageLocally(Bitmap imageBitmap) {
        try {
            // Guardar la imagen en la memoria interna del dispositivo
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);

            // Guardar la imagen en el archivo
            FileOutputStream fos = new FileOutputStream(imageFile);
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.close();

            // Obtener la URI utilizando FileProvider (actualizado para cumplir con las recomendaciones de Android)
            Uri imageUri = FileProvider.getUriForFile(this, "com.example.listadelacompra.fileprovider", imageFile);

            // Actualizar la variable imageUri con la URI del archivo guardado localmente
            this.imageUri = imageUri;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void subirImagen() {
        if (imageUri != null) {
            progressDialog.setMessage("Subiendo foto...");
            progressDialog.show();
            // Crear una referencia al archivo en Cloud Storage
            Random r = new Random();
            String extra = (""+r.nextInt(999999));
            StorageReference fileReference = storageReference.child(DIRECTORIO + "/" + System.currentTimeMillis()+extra + ".jpg");

            // Subir el archivo a Cloud Storage
            fileReference.putFile(imageUri)
                    .addOnSuccessListener(taskSnapshot -> {
                        progressDialog.dismiss();
                        Toast.makeText(Product_Activity.this, "Foto subida con éxito", Toast.LENGTH_SHORT).show();
                        // Obtén la URL de la imagen almacenada
                        fileReference.getDownloadUrl().addOnSuccessListener(uri -> {
                            // obtener la URL y almacenarla en tu base de datos
                            String imageUrl = uri.toString();
                            actualizarUrlEnBaseDeDatos(imageUrl);
                            cargarImagenDesdeRuta(imageUrl, findViewById(R.id.product_photo));
                        });
                    })
                    .addOnFailureListener(e -> {
                        progressDialog.dismiss();
                        Toast.makeText(Product_Activity.this, "Error al subir la foto", Toast.LENGTH_SHORT).show();
                    });
        }
    }
    private void subirImagenCapturada(Bitmap imageBitmap) {
        if (imageBitmap != null) {
            progressDialog.setMessage("Subiendo foto...");
            progressDialog.show();
            // Crear una referencia al archivo en Cloud Storage
            Random r = new Random();
            String extra = ("" + r.nextInt(999999));
            StorageReference fileReference = storageReference.child(DIRECTORIO + "/" + System.currentTimeMillis() + extra + ".jpg");

            // Convertir el Bitmap a un array de bytes (JPEG)
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            // Subir el array de bytes a Cloud Storage
            fileReference.putBytes(data)
                    .addOnSuccessListener(taskSnapshot -> {
                        progressDialog.dismiss();
                        Toast.makeText(Product_Activity.this, "Foto subida con éxito", Toast.LENGTH_SHORT).show();
                        fileReference.getDownloadUrl().addOnSuccessListener(uri -> {
                            // obtener la URL y almacenarla en tu base de datos
                            String imageUrl = uri.toString();
                            actualizarUrlEnBaseDeDatos(imageUrl);
                            cargarImagenDesdeRuta(imageUrl, findViewById(R.id.product_photo));
                        });
                    })
                    .addOnFailureListener(e -> {
                        progressDialog.dismiss();
                        Toast.makeText(Product_Activity.this, "Error al subir la foto", Toast.LENGTH_SHORT).show();
                    });
        }
    }


    private void actualizarUrlEnBaseDeDatos(String imageUrl) {
        ContentValues nuevosValores = new ContentValues();
        nuevosValores.put("rutaImagen",imageUrl);

        SQLiteDatabase db = stockProductos.getWritableDatabase();
        stockProductos.updateProducto(db, nuevosValores, idProducto);
        db.close();
    }

}