package com.example.listadelacompra;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AA_BuyList extends ArrayAdapter {

    Activity activity;
    ArrayList<Product> almacen = new ArrayList<>();
    MediaPlayer mp = MediaPlayer.create(this.getContext(), R.raw.latex_gloves);


    public AA_BuyList(Activity activity, ArrayList<Product> stock) {
        /** ---ACTIVITY-----------LAYOUT--------------ARRAY--**/
        super(activity, R.layout.product_3_buylist_view_item, stock);
        this.activity = activity;
        this.almacen = stock;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Obtiene el servicio de inflado de diseños desde la actividad
        LayoutInflater inflater = activity.getLayoutInflater();
        // Infla el diseño personalizado para cada elemento de la lista
        View item = inflater.inflate(R.layout.product_3_buylist_view_item, null);

        // Obtiene las referencias a los TextViews dentro del diseño inflado
        TextView productoNombre = item.findViewById(R.id.lblNombre);
        TextView productoDesc = item.findViewById(R.id.lblDescripcion);

        ImageView buttonDelete = item.findViewById(R.id.buttonDelete);
        ImageView miniatura = item.findViewById((R.id.mini_photoProduct));

        // Establece el texto de los TextViews con los datos del producto en la posición actual
        productoNombre.setText(almacen.get(position).getNombre().toString());
        productoDesc.setText(String.valueOf(almacen.get(position).getDescripcion()));

        cargarImagenDesdeRuta(almacen.get(position).getRutaImagen(), miniatura);
        /** ----------------------DELETE----------------------------**/
        //Button buttonDelete = item.findViewById(R.id.buttonDelete);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                almacen.remove(position);
                notifyDataSetChanged(); // notifica que se han cambiado datos
            }
        });
        /** ----------------------DELETE----------------------------**/

        //return super.getView(position, convertView, parent);
        return item;
    }

    private void cargarImagenDesdeRuta(String rutaImagen, ImageView imagen) {
        // Verificar si hay una ruta de imagen válida
        if (rutaImagen != null && !rutaImagen.isEmpty()) {
            Uri uri = Uri.parse(rutaImagen);
            Log.e("FOTO", uri.toString());
            Glide.with(activity)
                    .load(uri)
                    .fitCenter()  // Ajusta la escala de la imagen
                    .into(imagen);
        }
    }

}
