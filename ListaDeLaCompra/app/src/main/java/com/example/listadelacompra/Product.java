package com.example.listadelacompra;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Product implements Parcelable {
    private static final long serialVersionUID = 2L; // Incrementa al hacer cambios
    private int id;
    private String nombre;
    private String descripcion;
    private Double stockMinimo;
    private Double stockActual;
    private int idSql;
    private String rutaImagen = "";


    public Product(){}
    public Product(String nombre) {
        this.nombre = nombre;
    }

    public Product(String nombre, Double stockMinimo) {
        this.nombre = nombre;
        this.stockMinimo = stockMinimo;
    }
    public Product(String nombre, String descripcion, Double stockMinimo) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.stockMinimo = stockMinimo;
    }

    public Product(String nombre, String descripcion, Double stockMinimo, Double stockActual) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.stockMinimo = stockMinimo;
        this.stockActual = stockActual;
    }

    public Product(String nombre, String descripcion, Double stockMinimo, Double stockActual, int idSql) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.stockMinimo = stockMinimo;
        this.stockActual = stockActual;
        this.idSql = idSql;
    }
    public Product(String nombre, String descripcion, Double stockMinimo, Double stockActual, int idSql, String url) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.stockMinimo = stockMinimo;
        this.stockActual = stockActual;
        this.idSql = idSql;
        this.rutaImagen = url;
    }


    protected Product(Parcel in) {
        id = in.readInt();
        nombre = in.readString();
        descripcion = in.readString();
        if (in.readByte() == 0) {
            stockMinimo = null;
        } else {
            stockMinimo = in.readDouble();
        }
        if (in.readByte() == 0) {
            stockActual = null;
        } else {
            stockActual = in.readDouble();
        }
        idSql = in.readInt();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    /* CORREGIR CUANDO TENGA QUE HACER PEDIDOS */
    public Double calculaPedido(){
        Double pedido =(stockActual<stockMinimo)? stockMinimo - stockActual:0.0;
        return pedido;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(Double stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public Double getStockActual() {
        return stockActual;
    }

    public void setStockActual(Double stockActual) {
        this.stockActual = stockActual;
    }

    public int getIdSql() {
        return idSql;
    }

    public void setIdSql(int idSql) {
        this.idSql = idSql;
    }
    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }


    /**
     * Describe the kinds of special objects contained in this Parcelable
     * instance's marshaled representation. For example, if the object will
     * include a file descriptor in the output of {@link #writeToParcel(Parcel, int)},
     * the return value of this method must include the
     * {@link #CONTENTS_FILE_DESCRIPTOR} bit.
     *
     * @return a bitmask indicating the set of special object types marshaled
     * by this Parcelable object instance.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nombre);
        dest.writeString(descripcion);
        if (stockMinimo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(stockMinimo);
        }
        if (stockActual == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(stockActual);
        }
        dest.writeInt(idSql);
    }

}
