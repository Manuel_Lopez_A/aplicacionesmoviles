package com.example.listadelacompra;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class AA_StockList extends ArrayAdapter {

    Activity activity;
    ArrayList<Product> almacen = new ArrayList<>();

    public AA_StockList(Activity activity, ArrayList<Product> stock) {
        /** ---ACTIVITY-----------LAYOUT--------------ARRAY--**/
        super(activity, R.layout.product_2_stock_view_item, stock);
        this.activity = activity;
        this.almacen = stock;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Obtiene el servicio de inflado de diseños desde la actividad
        LayoutInflater inflater = activity.getLayoutInflater();
        // Infla el diseño personalizado para cada elemento de la lista
        View item = inflater.inflate(R.layout.product_2_stock_view_item, null);

        // Obtiene las referencias a los TextViews dentro del diseño inflado
        TextView productoNombre = item.findViewById(R.id.lblNombre);
        TextView productoDesc = item.findViewById(R.id.lblDescripcion);
        TextView productStock = item.findViewById(R.id.stockOfProduct);

        // Establece el texto de los TextViews con los datos del producto en la posición actual
        productoNombre.setText(almacen.get(position).getNombre().toString());
        productoDesc.setText(String.valueOf(almacen.get(position).getDescripcion()));
        productStock.setText(String.valueOf(almacen.get(position).getStockActual()));

        Button sumar = item.findViewById(R.id.buttonPlus);
        sumar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//              sumar 0.5
                double nuevoStock = almacen.get(position).getStockActual() + 0.5;
                almacen.get(position).setStockActual(nuevoStock);
                notifyDataSetChanged();
            }
        });

        Button restar = item.findViewById(R.id.buttonMinus);
        restar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//              restar 0.5
                double stockActual = almacen.get(position).getStockActual();
                if (stockActual >= 0.5) {
                    double nuevoStock = stockActual - 0.5;
                    almacen.get(position).setStockActual(nuevoStock);
                    notifyDataSetChanged();
                }
            }
        });

        //return super.getView(position, convertView, parent);
        return item;
    }
}
