package com.example.listadelacompra;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class Products_BuyList_Activity extends AppCompatActivity {
    private ArrayList<Product> almacen = new ArrayList<>();
    private AA_BuyList adaptador;
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("ListaDeLaCompra");
    MediaPlayer mp_delete_all;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3_buylist);
        adaptador = new AA_BuyList(this, almacen);
        ListView listaCompra = findViewById(R.id.listShopping);
        listaCompra.setAdapter(adaptador);
        // Crear el MediaPlayer en onCreate()
        mp_delete_all = MediaPlayer.create(this, R.raw.fastwosh);

        recuperarDatosFirebase();

        configurarBotones();
    }

    private void recuperarDatosFirebase() {
        DatabaseReference buyListRef = myRef.child("buyList");
        buyListRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                almacen.clear();
                for (DataSnapshot productSnapshot : dataSnapshot.getChildren()) {
                    Product product = productSnapshot.getValue(Product.class);
                    almacen.add(product);
                }
                adaptador.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Firebase", "Error al recuperar datos: " + databaseError.getMessage());
            }
        });
    }

    private void configurarBotones() {
        Button returnMain = findViewById(R.id.btnVaciar);
        returnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                estasSeguro();
            }
        });

        Button saveButton = findViewById(R.id.btnSaveList);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Actualizar la base de datos de Firebase con la lista actual
                actualizarFirebase();
                finish();
            }
        });
    }

    private void estasSeguro() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.advertenciaBorrado);
        builder.setMessage(R.string.mensajeBorrado);
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mp_delete_all.start();
                // Eliminar productos de la lista local y notificar al adaptador
                Iterator<Product> iterator = almacen.iterator();
                while (iterator.hasNext()) {
                    Product product = iterator.next();
                    if (product.getStockActual() <= 0) {
                        // Eliminar producto de la base de datos de Firebase
                        eliminarProductoFirebase(product);
                        iterator.remove();
                    }
                }
                adaptador.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void actualizarFirebase() {
        DatabaseReference buyListRef = myRef.child("buyList");
        buyListRef.setValue(almacen);
    }

    private void eliminarProductoFirebase(Product product) {
        DatabaseReference buyListRef = myRef.child("buyList").child(product.getNombre());
        buyListRef.removeValue();
    }
}