package com.example.listadelacompra;

import static android.app.PendingIntent.getActivity;


import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Locale;

public class Main_Activity extends AppCompatActivity {
    Productos_BBDD stockProductos = new Productos_BBDD(this, "stockProductos", null, 1);
    SharedPreferences sharedPref;
    String language = "en";
    String theme = "LIGHT";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_0_main);

        // VAMOS A METERLE DATOS AL INICIAR
        // iniciaBBDD();
        sharedPref  = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String lenguajeGuardado = sharedPref.getString("languagePref", "");
        // Si el idioma almacenado es diferente al idioma por defecto, reinicia la actividad
        if (!Locale.getDefault().getLanguage().equals(lenguajeGuardado)) {
            cambiaIdioma();
            recreate();
            return;
        }
        TextView saludo = findViewById(R.id.welcomeTitle);
        String bienvenida = getResources().getString(R.string.welcomeSaluteYou);
        String nombre = sharedPref.getString("namePref", "");
        saludo.setText(bienvenida + " " + nombre);

        // BOTÓN PRODUCTOS
        Button productBtn = findViewById(R.id.productsButton);
        productBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("BUTTON1", "show me products");
                Intent myIntent = new Intent(v.getContext(), Products_List_Activity.class);
                startActivity(myIntent);
            }
        });

        // BOTÓN STOCK
        Button stockBtn = findViewById(R.id.stockButton);
        stockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("BUTTON2", "show me stock");
                Intent myIntent = new Intent(v.getContext(), Products_Stock_Activity.class);
                startActivity(myIntent);
            }
        });

        // BOTÓN LISTA
        Button listBtn = findViewById(R.id.listButton);
        listBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("BUTTON3", "show me buy list");
                Intent myIntent = new Intent(v.getContext(), Products_BuyList_Activity.class);
                startActivity(myIntent);
            }
        });

        // BOTÓN CONFIG
        Button configBtn = findViewById(R.id.settingsButton);
        configBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("BUTTON4", "show me preferences");
                Intent myIntent = new Intent(v.getContext(), Preferences.class);
                startActivity(myIntent);
            }
        });

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        cambiaIdioma();
        recreate(); // restart para poner idioma en el main también
    }
    private void cambiaIdioma() {
        Locale locale;
        language = sharedPref.getString("languagePref", "");
        switch (language){
            case "en":
                locale = new Locale("en");
                break;
            case "es":
                locale = new Locale("es");
                break;
            case "ca":
                locale = new Locale("ca");
                break;
            default:
                locale = Locale.getDefault();
                break;
        }
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
        Log.e("idioma cambiado:", language);
    }


    private void iniciaBBDD() {
        SQLiteDatabase db = stockProductos.getWritableDatabase();
        if (db != null)
        {
            db.execSQL("INSERT INTO productos (nombre, descripcion, stockMin, stockActual) " +
                    "VALUES ('Espguetis', 'Espaguetis italianos', 0.50, 1.00)");
            Log.e("insertado", "ESPAGUETIS");
            db.execSQL("INSERT INTO productos (nombre, descripcion, stockMin, stockActual) " +
                    "VALUES ('Lechuga', 'Variedades de lechugas', 1.00, 1.00)");
            Log.e("insertado", "LECHUGAS");
            db.execSQL("INSERT INTO productos (nombre, descripcion, stockMin, stockActual) " +
                    "VALUES ('Setas', 'Variedad de setas', 2.00, 00.5)");
            Log.e("insertado", "SETAS");
        }
        Log.e("OK", "INSERCIONES");
    }

    private void ifNotExistsCreate(){
        SQLiteDatabase db = stockProductos.getWritableDatabase();
    }
}