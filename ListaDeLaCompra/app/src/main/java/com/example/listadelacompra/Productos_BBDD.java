package com.example.listadelacompra;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;


public class Productos_BBDD extends SQLiteOpenHelper {

    private final static String TABLA = "productos";
    String sqlCreacion = "CREATE TABLE " + TABLA+ " (" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nombre TEXT NOT NULL," +
            "descripcion TEXT," +
            "stockMin DOUBLE," +
            "stockActual DOUBLE," +
            "rutaImagen TEXT" +
            ");";

    public Productos_BBDD(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        //EN EL NAME ES EL NOMBRE DEL FICHERO DE LA BBDD QUE SE GUARDA EN LOCALHOST (VIENE DEL MAIN)
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SOLO SE EJECUTA CUANDO SE CREA LA BBDD
        db.execSQL(sqlCreacion);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
//      Se ejecuta cuando la versión de la BBDD que llamamos es superior a la actual
//      Nosotros borraremos la BBDD y crearemos la nueva, pero habría que hacer una migración
        db.execSQL("DROP TABLE IF EXISTS " + TABLA);
        db.execSQL(sqlCreacion); // <-------------- tb se puede hacer ---> onCreate(db);
    }
    public void insertProducto(SQLiteDatabase db, ContentValues valores){
        if (db != null){
            Log.e("Actualizado", "ENTRO en insert");
            db.insert(TABLA, null, valores);
            Log.e("Actualizado", "HECHO el insert");
        }
        db.close();
    }
    public void updateProducto(SQLiteDatabase db, ContentValues nuevosValores, int productId){
//        COMPROBACIÓN CAMPOS
//        for (String key : nuevosValores.keySet()) {
//            Log.e("Actualizado", "Columna: " + key + ", Valor: " + nuevosValores.get(key) + " idSql: "+ productId);
//        }
        db.update(TABLA, nuevosValores, "id = " + productId, null);
    }


    public ArrayList<Product> llenarStockProductos(SQLiteDatabase db) {
        ArrayList<Product> stock = new ArrayList<>();

        Cursor infoBBDD = null;
        if (db != null) {
            infoBBDD = db.rawQuery("SELECT * FROM " +TABLA, null);
        }
        if (infoBBDD != null && infoBBDD.moveToFirst()) {
            do {
//                Log.d("DEBUG", "StockMin obtenido: " + infoBBDD.getDouble(3));
//                Log.d("DEBUG", "StockActual obtenido: " + infoBBDD.getDouble(4));
                stock.add(new Product(
                        infoBBDD.getString(1),
                        infoBBDD.getString(2),
                        infoBBDD.getDouble(3),
                        infoBBDD.getDouble(4),
                        infoBBDD.getInt(0),
                        infoBBDD.getString(5)

                ));
            } while (infoBBDD.moveToNext());
        }
        return stock;
    }

    public void deleteProducto(SQLiteDatabase db, int idProducto) {
        if (db != null) {
            db.delete(TABLA, "id = " + idProducto, null);
        }
        db.close();
    }


}
