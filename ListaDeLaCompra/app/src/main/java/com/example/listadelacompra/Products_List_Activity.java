package com.example.listadelacompra;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.SearchView;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class Products_List_Activity extends AppCompatActivity {
    private ArrayList<Product> almacen = new ArrayList<>();
    private ArrayList<Product> almacen_aux = new ArrayList<>();
    private Productos_BBDD dbHelper;
    private ListView listProducts;
    private AA_ProductList productListAdapter;
    private SearchView searchView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1_products);

        dbHelper = new Productos_BBDD(this, "stockProductos", null, 1);

        productListAdapter = new AA_ProductList(this, almacen_aux, dbHelper);
        listProducts = findViewById(R.id.listProducts);
        listProducts.setAdapter(productListAdapter);

        searchView = findViewById(R.id.searchView);

        listProducts.setOnItemClickListener((arg0, arg1, arg2, arg3) -> {
            Intent intent = new Intent(getApplicationContext(), Product_Activity.class);
            Bundle bundle = new Bundle();
            Product selectedProduct = almacen_aux.get(arg2);

            bundle.putString("nombre", selectedProduct.getNombre());
            bundle.putString("descripcion", selectedProduct.getDescripcion());
            bundle.putDouble("stockMinimo", selectedProduct.getStockMinimo());
            bundle.putDouble("stockActual", selectedProduct.getStockActual());
            bundle.putInt("idSql", selectedProduct.getIdSql());
            bundle.putString("rutaImagen",selectedProduct.getRutaImagen());

            intent.putExtras(bundle);
            startActivityForResult(intent, 1); // 1 es un código de solicitud que puedes elegir
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                filterProducts(newText);
                return true;
            }
        });

        configurarBotones();
        actualizarAdapter();
    }

    private void filterProducts(String newText) {
        almacen_aux.clear();

        for (Product product : almacen) {
            if (product.getNombre().toLowerCase().contains(newText.toLowerCase())) {
                almacen_aux.add(product);
            }
        }
        productListAdapter.notifyDataSetChanged();
    }

    private void configurarBotones() {
        findViewById(R.id.btnAnyadirProduct).setOnClickListener(view -> {
            Intent anyadir = new Intent(getApplicationContext(), Product_AddNew_Activity.class);
            startActivityForResult(anyadir, 1); // 1 es un código de solicitud que puedes elegir
        });

        findViewById(R.id.btnVolver).setOnClickListener(view -> finish());
    }

    public void actualizarAdapter() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        almacen.clear();
        almacen.addAll(dbHelper.llenarStockProductos(db));
        db.close();

        filterProducts(searchView.getQuery().toString());
        productListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            actualizarAdapter();
        }
    }
}