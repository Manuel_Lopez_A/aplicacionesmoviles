package com.example.listadelacompra;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Products_Stock_Activity extends AppCompatActivity {
    ArrayList<Product> almacen = new ArrayList<>();
    Productos_BBDD stockProductos = new Productos_BBDD(this, "stockProductos", null, 1);
    /** -------------------FIREBASE---------------------**/
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("ListaDeLaCompra");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2_stock);

        /** -------------------ADPTADOR---------------------**/
        AA_StockList adaptador = new AA_StockList(this, almacen);
        ListView listaCompra = findViewById(R.id.listStock);
        listaCompra.setAdapter(adaptador);

        //BOTÓN GUARDAR Y VOLVER
        Button returnMain = findViewById(R.id.btnSaveAndReturn);
        returnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizarBaseDeDatos();
                finish();
            }
        });
        //BOTÓN GENERAR
        Button saveButton = findViewById(R.id.btnSaveStock);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                actualizarBaseDeDatos();
                ArrayList<Product> buyList = obtenerListaGenerada();
                DatabaseReference buyListRef = myRef.child("buyList");
                // añadelo a Firebase
                buyListRef.setValue(buyList);
                Intent intent = new Intent(view.getContext(), Products_BuyList_Activity.class);
                startActivity(intent);
            }
        });
    }

    private ArrayList<Product> obtenerListaGenerada() {
        ArrayList<Product>newBuyList = new ArrayList<>();
        for (Product producto : almacen) {
            Double cantidadAPedir = producto.calculaPedido();
           if ( cantidadAPedir>=1) newBuyList.add(producto);
            //Log.e("array", producto.getNombre() +" a pedir..."+ cantidadAPedir.toString());
        }
        return newBuyList;
    }

    private void actualizarBaseDeDatos() {
        SQLiteDatabase db = stockProductos.getWritableDatabase();
        for (Product product : almacen) {
            ContentValues nuevosValores = new ContentValues();
            nuevosValores.put("stockActual", product.getStockActual());
            int productId = product.getIdSql();
            stockProductos.updateProducto(db, nuevosValores, productId);
        }
        db.close();
    }

    @Override
    protected void onStart() {
        super.onStart();
        actualizarAdapter();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        actualizarAdapter();
    }

    private void actualizarAdapter() {
        SQLiteDatabase db = stockProductos.getWritableDatabase();
        almacen.clear();
        almacen.addAll(stockProductos.llenarStockProductos(db));
        db.close();
        // Notificar al adaptador que los datos han cambiado
        // ((AA_StockList) ((ListView) findViewById(R.id.listStock)).getAdapter()).notifyDataSetChanged();
        ListView listProducts = findViewById(R.id.listStock);
        AA_StockList productListAdapter = (AA_StockList) listProducts.getAdapter();
        productListAdapter.notifyDataSetChanged();
    }

}