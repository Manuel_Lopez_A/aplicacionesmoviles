package com.example.listadelacompra;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Product_AddNew_Activity extends AppCompatActivity {
    Productos_BBDD stockProductos = new Productos_BBDD(this, "stockProductos", null, 1);
    Button buttonReturn, buttonInsertProduct;
    MediaPlayer mp_add;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__newproduct);

        // BOTONES
        buttonReturn = findViewById(R.id.buttonVolver);
        buttonInsertProduct = findViewById(R.id.buttonInsertar);

        mp_add = MediaPlayer.create(this, R.raw.pop);

        // AGREGAR FUNCION A LOS BOTONES
        buttonReturn.setOnClickListener(v-> finish());
//        buttonUploadImage.setOnClickListener(v-> uploadPhoto());
        buttonInsertProduct.setOnClickListener(v-> {
                insertProduct();
                mp_add.start();
                finish();
        });


    }
    private void insertProduct() {

        EditText nombre = findViewById(R.id.datosNombre);
        EditText descripcion = findViewById(R.id.datosDescripcion);
        EditText stockMinimo = findViewById(R.id.datosStockMin);
        EditText stockActual = findViewById(R.id.datosStockActual);

        String name = nombre.getText().toString();
        String description = descripcion.getText().toString();
        String stockMinText = stockMinimo.getText().toString().trim();
        String stockActText = stockActual.getText().toString().trim();
        String urlImage = "";

        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getApplicationContext(), "Por favor, ingrese un nombre", Toast.LENGTH_SHORT).show();
            return;
        }

        Double stockMin;
        Double stockAct;
        try {
            stockMin = TextUtils.isEmpty(stockMinText) ? 0.0 : Double.parseDouble(stockMinText);
            stockAct = TextUtils.isEmpty(stockActText) ? 0.0 : Double.parseDouble(stockActText);
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Ingrese valores numéricos para Stock", Toast.LENGTH_SHORT).show();
            return;
        }

        ContentValues contentValues = new ContentValues();
        contentValues.put("nombre", name);
        contentValues.put("descripcion", description);
        contentValues.put("stockMin", stockMin);
        contentValues.put("stockActual", stockAct);
        contentValues.put("rutaImagen", urlImage);

        SQLiteDatabase db = stockProductos.getWritableDatabase();
        stockProductos.insertProducto(db, contentValues);

        Toast.makeText(getApplicationContext(), "Producto insertado con éxito", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
    }
}